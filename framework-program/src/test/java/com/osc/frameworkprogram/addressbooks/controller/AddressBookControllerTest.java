package com.osc.frameworkprogram.addressbooks.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.osc.frameworkprogram.addressbooks.dto.AddressBookDto;
import com.osc.frameworkprogram.addressbooks.service.AddressBookService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@Transactional
public class AddressBookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AddressBookService addressBookService;


    @Test
    @DisplayName("주소록 등록 테스트")
    public void createAddressBook_테스트() throws Exception {

        AddressBookDto addressBookDto = AddressBookDto.builder()
                .email("gdhong@email.com")
                .name("홍길동")
                .mobile("010-1234-5678")
                .company("오픈소스컨설팅")
                .department("임시부서")
                .position("사원")
                .memo("임시부서 직원입니다.")
                .build();

        String content = new ObjectMapper().writeValueAsString(addressBookDto);

        when(addressBookService.createAddressBook(addressBookDto)).thenReturn(addressBookDto);

        ResultActions resultActions = mockMvc.perform(post("/osc/addressBook")
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("success"))
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    @DisplayName("주소록 목록 조회 테스트")
    public void getAddressBooks_테스트() throws Exception {
        List<AddressBookDto> list = new ArrayList<>();
        AddressBookDto addressBookDto = AddressBookDto.builder()
                .email("gdhong@email.com")
                .name("홍길동")
                .mobile("010-1234-5678")
                .company("오픈소스컨설팅")
                .department("임시부서")
                .position("사원")
                .memo("임시부서 직원입니다.")
                .build();
        list.add(addressBookDto);
        list.add(addressBookDto);

        when(addressBookService.getAddressBooks()).thenReturn(list);

        ResultActions resultActions = mockMvc.perform(get("/osc/addressBook")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("success"))
                .andExpect(jsonPath("$.data", Matchers.hasSize(2)))
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    @DisplayName("주소록 단건 조회 테스트")
    public void getAddressBook_테스트() throws Exception {
        AddressBookDto addressBookDto = AddressBookDto.builder()
                .id(1L)
                .email("gdhong@email.com")
                .name("홍길동")
                .mobile("010-1234-5678")
                .company("오픈소스컨설팅")
                .department("임시부서")
                .position("사원")
                .memo("임시부서 직원입니다.")
                .build();
        Long id = 1L;

        when(addressBookService.getAddressBook(id)).thenReturn(addressBookDto);

        ResultActions resultActions = mockMvc.perform(get("/osc/addressBook/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("success"))
                .andExpect(jsonPath("$.data.email").value("gdhong@email.com"))
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    @DisplayName("주소록 수정 테스트")
    public void updateAddressBook_테스트() throws Exception {
        AddressBookDto addressBookDto = AddressBookDto.builder()
                .id(1L)
                .email("gdhong@email.com")
                .name("홍길동")
                .mobile("010-1234-5678")
                .company("오픈소스컨설팅")
                .department("임시부서")
                .position("사원")
                .memo("임시부서 직원입니다.")
                .build();
        Long id = 1L;

        String content = new ObjectMapper().writeValueAsString(addressBookDto);

        when(addressBookService.updateAddressBook(addressBookDto)).thenReturn(addressBookDto);

        ResultActions resultActions = mockMvc.perform(put("/osc/addressBook/{id}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(content)
                .accept(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("success"))
                .andDo(MockMvcResultHandlers.print());
    }


    @Test
    @DisplayName("주소록 삭제 테스트")
    public void deleteAddressBook_테스트() throws Exception {
        Long id = 1L;
        when(addressBookService.deleteAddressBook(id)).thenReturn("삭제되었습니다.");

        ResultActions resultActions = mockMvc.perform(delete("/osc/addressBook/{id}", id)
                .accept(MediaType.APPLICATION_JSON));

        resultActions
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("success"))
                .andExpect(jsonPath("$.data").value("삭제되었습니다."))
                .andDo(MockMvcResultHandlers.print());

    }

}
