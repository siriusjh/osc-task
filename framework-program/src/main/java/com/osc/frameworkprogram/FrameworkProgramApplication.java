package com.osc.frameworkprogram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrameworkProgramApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrameworkProgramApplication.class, args);
	}

}
