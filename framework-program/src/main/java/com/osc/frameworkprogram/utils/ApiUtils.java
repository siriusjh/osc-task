package com.osc.frameworkprogram.utils;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.http.HttpStatus;

public class ApiUtils {

    public static<T> ApiResult<T> success(T data) {
        return new ApiResult<>("success", data, null);
    }

    public static ApiResult<?> error(Throwable throwable, HttpStatus status) {
        return new ApiResult<>("errors", null, new ApiError(throwable, status));
    }

    public static ApiResult<?> error(String message, HttpStatus status) {
        return new ApiResult<>("errors", null, new ApiError(message, status));
    }

    public static class ApiError {
        private final String message;
        private final int status;

        ApiError(Throwable throwable, HttpStatus status) {
            this(throwable.getMessage(), status);
        }

        ApiError(String message, HttpStatus status) {
            this.message = message;
            this.status = status.value();
        }

        public String getMessage() {
            return message;
        }

        public int getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("messeage", message)
                    .append("status", status)
                    .toString();
        }
    }

    public static class ApiResult<T> {
        private final String status;
        private final T data;

        private final ApiError errors;

        private ApiResult(String status, T data, ApiError errors) {
            this.status = status;
            this.data = data;
            this.errors = errors;
        }

        public String getStatus() {
            return status;
        }

        public ApiError getErrors() {
            return errors;
        }

        public T getData() {
            return data;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                    .append("status", status)
                    .append("data", data)
                    .append("errors", errors)
                    .toString();
        }
    }
}
