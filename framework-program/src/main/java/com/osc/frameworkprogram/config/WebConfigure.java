package com.osc.frameworkprogram.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfigure {

    @Bean
    public OpenAPI openAPI() {

        Info info = new Info()
                .version("v1.0.0")
                .title("오픈소스컨설팅 주소록 과제")
                .description("오픈소스컨설팅 주소록 REST API 과제");

        return new OpenAPI()
                .info(info);
    }
}
