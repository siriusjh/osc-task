package com.osc.frameworkprogram.addressbooks.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@Schema(description = "주소록 DTO")
public class AddressBookDto {

    private Long id;

    @Schema(description = "이메일", nullable = false, example = "gdhong@email.com")
    @NotBlank(message = "이메일은 필수 파라미터입니다.")
    @Email(message = "이메일 형식이 아닙니다.")
    private String email;
    
    @Schema(description = "이름", nullable = false, example = "홍길동")
    @NotBlank(message = "이름은 필수 파라미터입니다.")
    private String name;
    
    @Schema(description = "휴대폰 번호", nullable = true, example = "010-1234-5678")
    @Pattern(regexp = "^\\d{2,3}-\\d{3,4}-\\d{4}$", message = "휴대폰 번호의 양식과 맞지 않습니다. 01x-xxx(x)-xxxx")
    private String mobile;
    
    @Schema(description = "회사명", nullable = true, example = "오픈소스컨설팅")
    private String company;
    
    @Schema(description = "부서", nullable = true, example = "임시부서")
    private String department;
    
    @Schema(description = "직위", nullable = true, example = "사원")
    private String position;
    
    @Schema(description = "메모", nullable = true, example = "임시부서 직원입니다.")
    private String memo;

    private Timestamp createDate;

    @Builder
    public AddressBookDto(Long id, String email, String name, String mobile, String company, String department, String position, String memo) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.mobile = mobile;
        this.company = company;
        this.department = department;
        this.position = position;
        this.memo = memo;
    }
}
