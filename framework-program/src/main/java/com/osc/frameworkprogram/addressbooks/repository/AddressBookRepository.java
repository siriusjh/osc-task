package com.osc.frameworkprogram.addressbooks.repository;

import com.osc.frameworkprogram.addressbooks.entity.AddressBook;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressBookRepository extends JpaRepository<AddressBook, Long> {
    AddressBook findByEmail(String email);
}
