package com.osc.frameworkprogram.addressbooks.controller;

import com.osc.frameworkprogram.addressbooks.dto.AddressBookDto;
import com.osc.frameworkprogram.addressbooks.service.AddressBookService;
import com.osc.frameworkprogram.utils.ApiUtils.ApiResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static com.osc.frameworkprogram.utils.ApiUtils.success;

@Tag(name = "주소록", description = "주소록 관련 API 입니다.")
@RestController
@RequiredArgsConstructor
@RequestMapping("/")
public class AddressBookController {

    private final AddressBookService addressBookService;

    @Operation(summary = "주소록 등록", description = "주소록 등록 API입니다.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(schema = @Schema(implementation = AddressBookDto.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = Error.class)))
    })
    @PostMapping("/osc/addressBook")
    public ApiResult<AddressBookDto> createAddressBook(@Parameter(description = "AddressBookDto 객체") @Valid @RequestBody AddressBookDto addressBookDto){
        return success(addressBookService.createAddressBook(addressBookDto));
    }

    @Operation(summary = "주소록 목록 조회", description = "주소록 목록 조회 API입니다.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(schema = @Schema(implementation = AddressBookDto.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = Error.class)))
    })
    @GetMapping("/osc/addressBook")
    public ApiResult<List<AddressBookDto>> getAddressBooks(){
        return success(addressBookService.getAddressBooks());
    }

    @Operation(summary = "주소록 단건 조회", description = "주소록 단건 조회 API입니다.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(schema = @Schema(implementation = AddressBookDto.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = Error.class)))
    })
    @GetMapping("/osc/addressBook/{id}")
    public ApiResult<AddressBookDto> getAddressBook(@Parameter(description = "주소록 id") @PathVariable Long id){
        return success(addressBookService.getAddressBook(id));
    }

    @Operation(summary = "주소록 수정", description = "주소록 수정 API입니다.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(schema = @Schema(implementation = AddressBookDto.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = Error.class)))
    })
    @PutMapping("/osc/addressBook/{id}")
    public ApiResult<AddressBookDto> updateAddressBook(@Parameter(description = "주소록 id") @PathVariable Long id,
                                                       @Parameter(description = "AddressBookDto 객체") @Valid @RequestBody AddressBookDto addressBookDto){
        addressBookDto.setId(id);
        return success(addressBookService.updateAddressBook(addressBookDto));
    }


    @Operation(summary = "주소록 삭제", description = "주소록 삭제 API입니다.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "success", content = @Content(schema = @Schema(implementation = AddressBookDto.class))),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = @Content(schema = @Schema(implementation = Error.class)))
    })
    @DeleteMapping("/osc/addressBook/{id}")
    public ApiResult<String> deleteAddressBook(@Parameter(description = "주소록 id") @PathVariable Long id){
        return success(addressBookService.deleteAddressBook(id));
    }

}
