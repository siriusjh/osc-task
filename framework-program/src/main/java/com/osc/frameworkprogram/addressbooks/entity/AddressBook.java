package com.osc.frameworkprogram.addressbooks.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@Table(name="AddressBook")
@NoArgsConstructor
public class AddressBook {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String email;
    private String name;
    private String mobile;
    private String company;
    private String department;
    private String position;
    private String memo;
    @CreationTimestamp
    private Timestamp createDate;
}
