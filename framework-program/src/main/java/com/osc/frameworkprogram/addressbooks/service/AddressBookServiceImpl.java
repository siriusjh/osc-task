package com.osc.frameworkprogram.addressbooks.service;

import com.osc.frameworkprogram.addressbooks.dto.AddressBookDto;
import com.osc.frameworkprogram.addressbooks.entity.AddressBook;
import com.osc.frameworkprogram.addressbooks.repository.AddressBookRepository;
import com.osc.frameworkprogram.errors.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class AddressBookServiceImpl implements AddressBookService {

    private final AddressBookRepository addressBookRepository;

    @Transactional
    @Override
    public AddressBookDto createAddressBook(AddressBookDto addressBookDto) {
        checkEmail(addressBookDto);
        return save(addressBookDto);
    }

    @Transactional(readOnly = true)
    @Override
    public List<AddressBookDto> getAddressBooks() {
        ModelMapper mapper = new ModelMapper();
        return addressBookRepository.findAll()
                .stream()
                .map(p -> mapper.map(p, AddressBookDto.class))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public AddressBookDto getAddressBook(Long id) {
        AddressBook addressBook = addressBookRepository.findById(id).orElseThrow(() -> new NotFoundException("해당 주소록을 찾을 수 없습니다."));
        return new ModelMapper().map(addressBook, AddressBookDto.class);
    }

    @Transactional
    @Override
    public AddressBookDto updateAddressBook(AddressBookDto addressBookDto) {
        checkEmail(addressBookDto);
        return save(addressBookDto);
    }

    @Transactional
    @Override
    public String deleteAddressBook(Long id) {
        addressBookRepository.deleteById(id);
        return "삭제되었습니다.";
    }

    // 이메일 중복 체크
    private void checkEmail(AddressBookDto addressBookDto) {
        AddressBook addressBook = addressBookRepository.findByEmail(addressBookDto.getEmail());
        if(!ObjectUtils.isEmpty(addressBook)) {
            throw new IllegalArgumentException("이미 등록된 이메일 입니다.");
        }
    }

    // 주소록 저장
    private AddressBookDto save(AddressBookDto addressBookDto) {
        ModelMapper mapper = new ModelMapper();
        //mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        AddressBook addressBook = mapper.map(addressBookDto, AddressBook.class);
        return new ModelMapper().map(addressBookRepository.save(addressBook), AddressBookDto.class);
    }
}
