package com.osc.frameworkprogram.addressbooks.service;

import com.osc.frameworkprogram.addressbooks.dto.AddressBookDto;

import java.util.List;

public interface AddressBookService {

    AddressBookDto createAddressBook(AddressBookDto addressBookDto);
    List<AddressBookDto> getAddressBooks();
    AddressBookDto getAddressBook(Long id);
    AddressBookDto updateAddressBook(AddressBookDto addressBookDto);
    String deleteAddressBook(Long id);
}
