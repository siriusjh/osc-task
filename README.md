# 백엔드 엔지니어 과제

## 1. Framework 프로그램 - port: 8080

### 1) 빌드 방법:
1. git clone https://gitlab.com/siriusjh/osc-task.git
2. cd osc-task/framework-program
3. ./gradlew.bat build
4. java -jar build/libs/framework-program-0.0.1-SNAPSHOT.jar

### 2) swagger 주소 :  [http://localhost:8080/swagger-ui/index.html](http://localhost:8080/swagger-ui/index.html)

### 3) Api 실행 순서
1. 주소록 등록  
2. 주소록 목록 조회 
3. 주소록 단건 조회  
4. 주소록 수정  
5. 주소록 삭제  

### 4) 검증 : AddressBookController junit 단위 테스트 success
- [x] 주소록 등록 테스트 : createAddressBook_테스트()
- [x] 주소록 목록 조회 테스트 : getAddressBooks_테스트()
- [x] 주소록 단건 조회 테스트 : getAddressBook_테스트()
- [x] 주소록 수정 테스트 : updateAddressBook_테스트()
- [x] 주소록 삭제 테스트 : deleteAddressBook_테스트()

## 2. Java  프로그램 - socket port: 8888
### * Master, Slave 각각 다른 terminal에서 동시에 실행
### 1) 빌드 방법: Master 프로그램
1. cd osc-task/master
2. ./gradlew.bat build
3. java -jar build/libs/master-1.0-SNAPSHOT.jar

### 2) 빌드 방법: Slave 프로그램
1. cd osc-task/master
2. ./gradlew.bat build
3. java -jar build/libs/slave-1.0-SNAPSHOT.jar

