package org.task;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {

    private static final int PORT = 8888;
    private static final String FILE_PATH = "./data.txt";
    private static final BlockingQueue<String> bq = new LinkedBlockingQueue<>();

    public static void main(String[] args) {
        new Thread(new DataGenerator()).start();
        new Thread(new DataLoader()).start();
        new Thread(new DataSender()).start();
    }

    private static class DataGenerator implements Runnable {

        @Override
        public void run() {
            try {
                PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(FILE_PATH, true)));
                while(true) {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    int randomNumber = new Random().nextInt(100);

                    pw.println(timestamp + ", " + randomNumber);
                    pw.flush();

                    File file = new File(FILE_PATH);
                    long fileSize = file.length();
                    if(fileSize > 1000 * 1024) {
                        pw.close();
                        return;
                    }
                    Thread.sleep(100);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static class DataLoader implements Runnable {
        @Override
        public void run() {
            try {
                BufferedReader br = new BufferedReader(new FileReader(FILE_PATH));
                String line;
                while((line = br.readLine()) != null) {
                    bq.put(line);
                    Thread.sleep(1000);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static class DataSender implements Runnable {

        @Override
        public void run() {
            try {
                ServerSocket serverSocket = new ServerSocket(PORT);
                while(true) {
                    Socket socket = serverSocket.accept();
                    new Thread(new DataTransmitter(socket.getOutputStream())).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static class DataTransmitter implements Runnable {
        private OutputStream outputStream;
        public DataTransmitter(OutputStream outputStream) {
            this.outputStream = outputStream;
        }
        @Override
        public void run() {
            try {

                PrintWriter pw = new PrintWriter(outputStream, true);

                while(true) {
                    String data = bq.take();
                    System.out.println(data);
                    pw.println(data);
                    Thread.sleep(1000);
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}