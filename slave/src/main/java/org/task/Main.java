package org.task;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    private static final String HOST = "127.0.0.1";
    private static final int PORT = 8888;
    private static final String FILE_PATH = "./received_data.txt";
    private static final BlockingQueue<String> bq = new LinkedBlockingQueue<>();

    public static void main(String[] args) {
        new Thread(new DataWriter()).start();
        new Thread(new DataReceiver()).start();
    }

    private static class DataWriter implements Runnable {

        @Override
        public void run() {
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_PATH, true));

                while(true) {
                    String data = bq.take();
                    System.out.println(data);
                    bw.write(data);
                    bw.newLine();
                    bw.flush();
                }
            } catch (IOException | InterruptedException e)  {
                e.printStackTrace();
            }
        }
    }

    private static class DataReceiver implements Runnable {

        @Override
        public void run() {
            try {
                Socket socket = new Socket(HOST, PORT);
                BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                while(true) {
                    String data = br.readLine();
                    bq.offer(data);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}